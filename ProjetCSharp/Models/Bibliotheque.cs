namespace ProjetCSharp.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Bibliotheque : DbContext
    {
        public Bibliotheque()
            : base("name=Bibliotheque")
        {
        }

        public virtual DbSet<Auteurs> Auteurs { get; set; }
        public virtual DbSet<Emprunts> Emprunts { get; set; }
        public virtual DbSet<Exemplaires> Exemplaires { get; set; }
        public virtual DbSet<Livres> Livres { get; set; }
        public virtual DbSet<Membres> Membres { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Auteurs>()
                .HasMany(e => e.Livres)
                .WithRequired(e => e.Auteurs)
                .HasForeignKey(e => e.id_auteur)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Exemplaires>()
                .HasMany(e => e.Emprunts)
                .WithRequired(e => e.Exemplaires)
                .HasForeignKey(e => e.id_exemplaire)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Livres>()
                .HasMany(e => e.Exemplaires)
                .WithRequired(e => e.Livres)
                .HasForeignKey(e => e.id_livre)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Membres>()
                .HasMany(e => e.Emprunts)
                .WithRequired(e => e.Membres)
                .HasForeignKey(e => e.id_membre)
                .WillCascadeOnDelete(false);
        }
    }
}
