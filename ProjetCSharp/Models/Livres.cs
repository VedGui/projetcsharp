namespace ProjetCSharp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Livres
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Livres()
        {
            Exemplaires = new HashSet<Exemplaires>();
        }

        public int id { get; set; }

        [Required]
        [StringLength(50)]
        public string titre { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string synopsis { get; set; }

        public string image { get; set; }

        public int id_auteur { get; set; }

        public virtual Auteurs Auteurs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Exemplaires> Exemplaires { get; set; }
    }
}
