namespace ProjetCSharp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Emprunts
    {
        public int id { get; set; }

        public DateTime debut { get; set; }

        public DateTime fin { get; set; }

        public bool fini { get; set; }

        public int id_exemplaire { get; set; }

        public int id_membre { get; set; }

        public virtual Exemplaires Exemplaires { get; set; }

        public virtual Membres Membres { get; set; }
    }
}
