namespace ProjetCSharp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Exemplaires
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Exemplaires()
        {
            Emprunts = new HashSet<Emprunts>();
        }

        public int id { get; set; }

        public int id_livre { get; set; }

        public string idExemplaireTitre
        {
            get
            {
                return id + " " + Livres.titre;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Emprunts> Emprunts { get; set; }

        public virtual Livres Livres { get; set; }
    }
}
