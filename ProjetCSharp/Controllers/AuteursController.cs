﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjetCSharp.Models;

namespace ProjetCSharp.Controllers
{
    public class AuteursController : Controller
    {
        private Bibliotheque db = new Bibliotheque();

        // GET: Auteurs
        public ActionResult Index()
        {
            return View(db.Auteurs.ToList());
        }

        // GET: Auteurs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auteurs auteurs = db.Auteurs.Find(id);
            if (auteurs == null)
            {
                return HttpNotFound();
            }
            return View(auteurs);
        }

        // GET: Auteurs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Auteurs/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,nom,prenom,dateNaissance")] Auteurs auteurs)
        {
            if (ModelState.IsValid)
            {
                db.Auteurs.Add(auteurs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(auteurs);
        }

        // GET: Auteurs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auteurs auteurs = db.Auteurs.Find(id);
            if (auteurs == null)
            {
                return HttpNotFound();
            }
            return View(auteurs);
        }

        // POST: Auteurs/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,nom,prenom,dateNaissance")] Auteurs auteurs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(auteurs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(auteurs);
        }

        // GET: Auteurs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auteurs auteurs = db.Auteurs.Find(id);
            if (auteurs == null)
            {
                return HttpNotFound();
            }
            return View(auteurs);
        }

        // POST: Auteurs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Auteurs auteurs = db.Auteurs.Find(id);
            db.Auteurs.Remove(auteurs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
