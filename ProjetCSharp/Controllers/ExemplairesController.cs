﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjetCSharp.Models;

namespace ProjetCSharp.Controllers
{
    public class ExemplairesController : Controller
    {
        private Bibliotheque db = new Bibliotheque();

        // GET: Exemplaires
        public ActionResult Index()
        {
            var exemplaires = db.Exemplaires.Include(e => e.Livres);
            return View(exemplaires.ToList());
        }

        // GET: Exemplaires/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Exemplaires exemplaires = db.Exemplaires.Find(id);
            if (exemplaires == null)
            {
                return HttpNotFound();
            }
            return View(exemplaires);
        }

        // GET: Exemplaires/Create
        public ActionResult Create()
        {
            ViewBag.id_livre = new SelectList(db.Livres, "id", "titre");
            return View();
        }

        // POST: Exemplaires/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_livre")] Exemplaires exemplaires)
        {
            if (ModelState.IsValid)
            {
                db.Exemplaires.Add(exemplaires);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_livre = new SelectList(db.Livres, "id", "titre", exemplaires.id_livre);
            return View(exemplaires);
        }

        // GET: Exemplaires/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Exemplaires exemplaires = db.Exemplaires.Find(id);
            if (exemplaires == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_livre = new SelectList(db.Livres, "id", "titre", exemplaires.id_livre);
            return View(exemplaires);
        }

        // POST: Exemplaires/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_livre")] Exemplaires exemplaires)
        {
            if (ModelState.IsValid)
            {
                db.Entry(exemplaires).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_livre = new SelectList(db.Livres, "id", "titre", exemplaires.id_livre);
            return View(exemplaires);
        }

        // GET: Exemplaires/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Exemplaires exemplaires = db.Exemplaires.Find(id);
            if (exemplaires == null)
            {
                return HttpNotFound();
            }
            return View(exemplaires);
        }

        // POST: Exemplaires/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Exemplaires exemplaires = db.Exemplaires.Find(id);
            db.Exemplaires.Remove(exemplaires);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
