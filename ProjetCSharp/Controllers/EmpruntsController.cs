﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjetCSharp.Models;

namespace ProjetCSharp.Controllers
{
    public class EmpruntsController : Controller
    {
        private Bibliotheque db = new Bibliotheque();

        // GET: Emprunts
        public ActionResult Index()
        {
            var emprunts = db.Emprunts.Include(e => e.Exemplaires).Include(e => e.Membres);
            return View(emprunts.ToList());
        }

        // GET: Emprunts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emprunts emprunts = db.Emprunts.Find(id);
            if (emprunts == null)
            {
                return HttpNotFound();
            }
            return View(emprunts);
        }

        // GET: Emprunts/Create
        public ActionResult Create()
        {
            var dbb = db.Exemplaires;
            var a = db.Emprunts.Where(x => x.fini == false);
            var b = dbb.Except(a.Select(x => x.Exemplaires));
            ViewBag.id_exemplaire = new SelectList(b, "id", "idExemplaireTitre");
            ViewBag.id_membre = new SelectList(db.Membres, "id", "nomPrenom");
            return View();
        }

        // POST: Emprunts/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,debut,fin,id_exemplaire,id_membre")] Emprunts emprunts)
        {
            if (ModelState.IsValid)
            {
                db.Emprunts.Add(emprunts);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_exemplaire = new SelectList(db.Exemplaires, "id", "id", emprunts.id_exemplaire);
            ViewBag.id_membre = new SelectList(db.Membres, "id", "nomPrenom", emprunts.id_membre);
            return View(emprunts);
        }

        // GET: Emprunts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emprunts emprunts = db.Emprunts.Find(id);
            if (emprunts == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_exemplaire = new SelectList(db.Exemplaires, "id", "id", emprunts.id_exemplaire);
            ViewBag.id_membre = new SelectList(db.Membres, "id", "nom", emprunts.id_membre);
            return View(emprunts);
        }

        // POST: Emprunts/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,debut,fin,fini,id_exemplaire,id_membre")] Emprunts emprunts)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emprunts).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_exemplaire = new SelectList(db.Exemplaires, "id", "id", emprunts.id_exemplaire);
            ViewBag.id_membre = new SelectList(db.Membres, "id", "nom", emprunts.id_membre);
            return View(emprunts);
        }

        // GET: Emprunts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emprunts emprunts = db.Emprunts.Find(id);
            if (emprunts == null)
            {
                return HttpNotFound();
            }
            return View(emprunts);
        }

        // POST: Emprunts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Emprunts emprunts = db.Emprunts.Find(id);
            db.Emprunts.Remove(emprunts);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Emprunts/Retour/5
        public ActionResult Retour(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Emprunts emprunts = db.Emprunts.Find(id);
            if (emprunts == null)
            {
                return HttpNotFound();
            }
            return View(emprunts);
        }

        // POST: Emprunts/Retour/5
        [HttpPost, ActionName("Retour")]
        [ValidateAntiForgeryToken]
        public ActionResult RetourConfirmed(int id)
        {
            Emprunts emprunts = db.Emprunts.Find(id);
            emprunts.fini = true;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
